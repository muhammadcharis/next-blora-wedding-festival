import type { NextRequest } from 'next/server';
import createIntlMiddleware from 'next-intl/middleware';

import { appConfig } from './app.config';
import updateSession from './libs/superbase/middleware';

const handleI18nRouting = createIntlMiddleware(appConfig.i18n);

const middleware = async (request: NextRequest) => {
  const response = handleI18nRouting(request);

  await updateSession(request);
  return response;
};

export default middleware;
export const config = {
  matcher: ['/((?!.+\\.[\\w]+$|_next).*)', '/', '/(api|trpc)(.*)'],
};
