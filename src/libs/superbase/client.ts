import { createBrowserClient } from '@supabase/ssr';

import { env } from '@/env.mjs';

const createClient = () => {
  const { NEXT_PUBLIC_SUPABASE_ANON_KEY, NEXT_PUBLIC_SUPABASE_URL } = env;
  return createBrowserClient(
    NEXT_PUBLIC_SUPABASE_URL,
    NEXT_PUBLIC_SUPABASE_ANON_KEY!,
  );
};

export { createClient };
