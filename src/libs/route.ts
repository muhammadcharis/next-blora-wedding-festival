import 'server-only';

import { NextResponse } from 'next/server';

import type { ErrorWithStatus } from '@/libs/fetch';

export const createResponseError = (err: ErrorWithStatus): NextResponse => {
  const { message, status, error } = err;

  return NextResponse.json({ error, message }, { status });
};
