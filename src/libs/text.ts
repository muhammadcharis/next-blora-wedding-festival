const capitalizeEachWord = (sentence: string) => {
  return sentence
    .toLowerCase()
    .replace(/(^|\s)\S/g, function capitalizeFirstLetter(firstLetter) {
      return firstLetter.toUpperCase();
    });
};

const maskEmail = (email: string) => {
  const [localPart, domain] = email.split('@');
  const maskedLocalPart =
    localPart.slice(0, 3) +
    localPart.slice(localPart.length - 9).replace(/./g, '*');
  return `${maskedLocalPart}@${domain}`;
};

const maskPhoneNumber = (phoneNumber: string) => {
  const visibleDigits = 8;
  const maskedDigits =
    phoneNumber.slice(0, visibleDigits) +
    '*'.repeat(phoneNumber.length - visibleDigits);
  return maskedDigits;
};

export { capitalizeEachWord, maskEmail, maskPhoneNumber };
