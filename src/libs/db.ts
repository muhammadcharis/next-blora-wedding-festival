import Dexie, { type EntityTable } from 'dexie';

import type { Rsvp } from '@/app/[locale]/(registration)/_schemas/rsvp';

export const db = new Dexie('bwf') as Dexie & {
  rsvp: EntityTable<Rsvp, 'id'>;
};

const fields = [
  'id',
  'rsvpUrl',
  'name',
  'phoneNumber',
  'email',
  'weedingPlanDate',
  'address',
  'createdAt',
];

db.version(1).stores({ rsvp: fields.join(',') });
