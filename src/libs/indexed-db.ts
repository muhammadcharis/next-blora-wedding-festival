const version = 1;

export const DB_NAME = 'bwf';

export enum Stores {
  Rsvp = 'rsvp',
}

export const initDb = (): Promise<IDBDatabase> => {
  return new Promise((resolve, reject) => {
    const request = indexedDB.open(DB_NAME, version);

    request.onupgradeneeded = (_event) => {
      const db = request.result;

      if (!db.objectStoreNames.contains(Stores.Rsvp)) {
        db.createObjectStore(Stores.Rsvp, {
          keyPath: 'id',
        });
      }
    };

    request.onsuccess = () => {
      resolve(request.result);
    };

    request.onerror = () => {
      reject(new Error('Database failed to open'));
    };
  });
};
