import type { LocalePrefix } from 'node_modules/next-intl/dist/types/src/shared/types';

const localePrefix: LocalePrefix = 'as-needed';
const locales = ['id', 'en'];
const defaultLocale = locales[0]!;
const localeDetection = false;

export const appConfig = {
  i18n: { locales, defaultLocale, localePrefix, localeDetection },
  // TODO: Add other config here
};
