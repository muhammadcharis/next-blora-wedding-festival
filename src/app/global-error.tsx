'use client';

import Error from 'next/error';

const GlobalError = (props: { params: { locale: string } }) => {
  return (
    <html lang={props.params.locale}>
      <body>
        {/* This is the default Next.js error component but it doesn't allow omitting the statusCode property yet. */}
        <Error statusCode={undefined as never} />
      </body>
    </html>
  );
};

export default GlobalError;
