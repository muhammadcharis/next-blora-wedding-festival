'use client';

import {
  createContext,
  useCallback,
  useContext,
  useMemo,
  useState,
} from 'react';

interface RegisterContextProps {
  isShowWelcomeMessage: boolean;
  handleSetIsShowWelcomeMessage: (isShowMessage: boolean) => void;
}

const RegisterContext = createContext<RegisterContextProps | undefined>(
  undefined,
);

export const useRegisters = () => {
  const context = useContext(RegisterContext);
  if (!context) {
    throw new Error('useRegisters must be used within a UsersProvider');
  }
  return context;
};

const RegisterProvider = ({
  children,
}: {
  children: Readonly<React.ReactNode>;
}) => {
  const [isShowWelcomeMessage, setIsShowWelcomeMessage] =
    useState<boolean>(false);

  const handleSetIsShowWelcomeMessage = useCallback(
    (newIsShowWelcomeMessage: boolean) => {
      setIsShowWelcomeMessage(newIsShowWelcomeMessage);
    },
    [],
  );

  const value = useMemo(
    () => ({
      isShowWelcomeMessage,
      handleSetIsShowWelcomeMessage,
    }),
    [isShowWelcomeMessage, handleSetIsShowWelcomeMessage],
  );

  return (
    <RegisterContext.Provider value={value}>
      {children}
    </RegisterContext.Provider>
  );
};

export { RegisterProvider };
