import Image from 'next/image';

import { Wrapper } from './_components/atoms/wrapper';
import { AudioPlayer } from './_components/audio-player';
import { ButtonWelcome } from './_components/button-welcome';
import { FooterSection } from './_components/footer-section';
import { FormSection } from './_components/form-section';
import { RundownSection } from './_components/rundown-section';
import { WelcomeSection } from './_components/welcome-section';
import { RegisterProvider } from './_context/register-context';

const Home = () => {
  return (
    <RegisterProvider>
      <div className="bwf-relative bwf-h-full">
        <video
          autoPlay
          className="bwf-absolute bwf-left-0 bwf-top-0 bwf-h-full bwf-w-full bwf-object-cover"
          loop
          muted
        >
          <source src="/videos/welcome.mp4" type="video/mp4" />
          Your browser does not support the video tag.
        </video>
        <Image
          alt="top-left-title"
          className="bwf-absolute bwf-left-0 bwf-top-0"
          height={200}
          src="/images/top-left-title.png"
          width={220}
        />
        <div className="bwf-flex bwf-flex-row bwf-justify-end bwf-gap-5 bwf-bg-header-gradient bwf-p-11">
          <Image
            alt="culture"
            height={70}
            src="/images/culture.png"
            width={60}
          />
          <Image alt="mac" height={70} src="/images/mac.png" width={60} />
        </div>

        <div className="!bwf-absolute !bwf-inset-x-0 !bwf-bottom-[70px] bwf-flex bwf-items-center bwf-justify-center ">
          <ButtonWelcome />
        </div>
        <Image
          alt="bottom-right-title"
          className="bwf-absolute bwf-bottom-0 bwf-right-0"
          height={200}
          src="/images/sv-bottom-right.png"
          width={220}
        />
      </div>
      <Wrapper>
        <WelcomeSection />
        <RundownSection />
        <FormSection />
        <FooterSection />
        <AudioPlayer />
      </Wrapper>
    </RegisterProvider>
  );
};

export default Home;
