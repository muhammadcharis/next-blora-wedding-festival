import { Ticket } from '@/app/[locale]/(registration)/_components/ticket';

const Page = () => {
  return (
    <div className="bwf-relative bwf-h-full">
      <Ticket />
    </div>
  );
};

export default Page;
