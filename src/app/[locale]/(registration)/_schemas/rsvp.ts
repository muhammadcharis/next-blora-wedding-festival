import { z } from 'zod';

const phoneIndonesiaRegex = /^(\+62|62|0)8[1-9][0-9]{6,11}$/;

export const RsvpSchema = z.object({
  name: z
    .string()
    .max(100, { message: 'rsvp.error.name.max' })
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'rsvp.error.name.required',
      },
    ),
  phoneNumber: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'rsvp.error.phoneNumber.required',
      },
    )
    .refine(
      (value) => {
        return phoneIndonesiaRegex.test(value!);
      },
      {
        message: 'rsvp.error.phoneNumber.format',
      },
    ),
  email: z
    .string()
    .email({ message: 'rsvp.error.email.format' })
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'rsvp.error.email.required',
      },
    ),
  weedingPlanDate: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'rsvp.error.weedingPlanDate.required',
      },
    ),
  address: z
    .string()
    .optional()
    .refine(
      (value) => {
        return value !== undefined && value.trim() !== '';
      },
      {
        message: 'rsvp.error.address.required',
      },
    ),
});

export type Rsvp = z.infer<typeof RsvpSchema> & {
  id: string;
  rsvpUrl: string;
};
