const Layout = ({ children }: { children: Readonly<React.ReactNode> }) => {
  return (
    <main className="bwf-relative bwf-mx-auto bwf-min-h-dvh bwf-w-full bwf-bg-white md:bwf-max-w-lg">
      {children}
    </main>
  );
};

export default Layout;
