'use client';

import { Image as AntdImage } from 'antd';
import Image from 'next/image';
import { useTranslations } from 'next-intl';
import { useEffect, useRef } from 'react';

import { useRegisters } from '../_context/register-context';

const WelcomeSection = () => {
  const { isShowWelcomeMessage } = useRegisters();
  const t = useTranslations('global');
  const welcomeSectionRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    if (isShowWelcomeMessage && welcomeSectionRef.current) {
      welcomeSectionRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  }, [isShowWelcomeMessage, welcomeSectionRef]);

  return (
    <section
      ref={welcomeSectionRef}
      className="bwf-min-h-svh bwf-bg-[url('/images/form-rsvp.jpg')]"
    >
      <div className="bwf-flex bwf-flex-col bwf-items-center bwf-justify-start bwf-border-y-4 bwf-border-y-gold bwf-bg-[url('/images/bg-green.webp')] bwf-px-4 bwf-py-8 bwf-text-white">
        <p className="bwf-text-base">{t('welcome')}</p>
        <p className="bwf-text-lg bwf-font-bold">{t('greeting')}</p>
      </div>
      <div className="bwf-flex bwf-flex-col bwf-items-center bwf-justify-center bwf-px-14 bwf-py-8">
        <Image alt="dahayu" height={200} src="/images/dahayu.png" width={200} />
        <p className="bwf-break-words bwf-text-center bwf-leading-relaxed">
          Diah Kharisma Wedding Bazaar mengangkat
          <b>
            <i>“Culture &amp; Colour Of Dahayu”</i>
          </b>
          sebagai tema DKRWB di tahun 2023. Tema yang terinspirasi dari budaya
          dan warna-warni yang menghiasi pernikahan tradisional Jawa. Dikemas
          secara modern, dengan icon “KERIS” yang sangat lekat dengan budaya
          Pernikahan Tradisional Jawa.
        </p>
        <div className="bwf-my-8 bwf-flex bwf-flex-col bwf-items-center bwf-justify-center bwf-gap-2">
          <h1 className="bwf-text-2xl bwf-font-bold ">{t('boothPlan')}</h1>
          <p className="bwf-text-sm">
            {t.rich('welcomeDesc', {
              // eslint-disable-next-line react/no-unstable-nested-components
              important: (chunks) => <b>80+{chunks}</b>,
            })}
          </p>
        </div>
        <AntdImage className="bwf-rounded-2xl" src="/images/layout.webp" />
      </div>
    </section>
  );
};
export { WelcomeSection };
