import Image from 'next/image';

const FooterSection = () => {
  return (
    <section className="bwf-relative bwf-bg-[url('/images/bg-green.webp')] bwf-bg-cover bwf-bg-center bwf-p-11">
      <Image
        alt="top-left-title"
        className="bwf-absolute bwf-left-0 bwf-top-0"
        height={200}
        src="/images/sv-top-left.png"
        width={220}
      />
      <div className="bwf-relative bwf-z-10 bwf-flex bwf-flex-col bwf-items-center bwf-justify-center bwf-gap-4 bwf-pt-20 bwf-text-center bwf-text-white">
        <p className="bwf-font-bold">6 - 9 APRIL 2023</p>
        <p className="bwf-text-lg">
          WEDDING BAZAAR | CULINARY BAZAAR | CREATIVE ACTIVITY
        </p>
        <div>
          <p className="bwf-text-sm bwf-font-light">Organized by:</p>
          <Image
            alt="logo"
            className="bwf-invert"
            height={150}
            src="/images/rwb.png"
            width={150}
          />
        </div>
        <div className="bwf-flex bwf-flex-col bwf-gap-4">
          <p className="bwf-text-sm bwf-font-light">Supported by:</p>
          <div className="bwf-grid bwf-grid-cols-3 bwf-gap-3">
            <div className="bwf-flex bwf-h-14 bwf-items-center bwf-justify-center bwf-rounded-md bwf-bg-white bwf-p-1">
              <Image
                alt="logo"
                className="bwf-max-w-full"
                height={40}
                src="/images/logo-nasmoco.png"
                width={200}
              />
            </div>
            <div className="bwf-flex bwf-h-14 bwf-items-center bwf-justify-center bwf-rounded-md bwf-bg-white bwf-p-1">
              <Image
                alt="logo"
                className="bwf-max-w-full"
                height={40}
                src="/images/logo-nasmoco.png"
                width={200}
              />
            </div>
            <div className="bwf-flex bwf-h-14 bwf-items-center bwf-justify-center bwf-rounded-md bwf-bg-white bwf-p-1">
              <Image
                alt="logo"
                className="bwf-max-w-full"
                height={40}
                src="/images/logo-nasmoco.png"
                width={200}
              />
            </div>
            <div className="bwf-flex bwf-h-14 bwf-items-center bwf-justify-center bwf-rounded-md bwf-bg-white bwf-p-1">
              <Image
                alt="logo"
                className="bwf-max-w-full"
                height={40}
                src="/images/logo-nasmoco.png"
                width={200}
              />
            </div>
            <div className="bwf-flex bwf-h-14 bwf-items-center bwf-justify-center bwf-rounded-md bwf-bg-white bwf-p-1">
              <Image
                alt="logo"
                className="bwf-max-w-full"
                height={40}
                src="/images/logo-nasmoco.png"
                width={200}
              />
            </div>
            <div className="bwf-flex bwf-h-14 bwf-items-center bwf-justify-center bwf-rounded-md bwf-bg-white bwf-p-1">
              <Image
                alt="logo"
                className="bwf-max-w-full"
                height={40}
                src="/images/logo-nasmoco.png"
                width={200}
              />
            </div>
            <div className="bwf-flex bwf-h-14 bwf-items-center bwf-justify-center bwf-rounded-md bwf-bg-white bwf-p-1">
              <Image
                alt="logo"
                className="bwf-max-w-full"
                height={40}
                src="/images/logo-nasmoco.png"
                width={200}
              />
            </div>
            <div className="bwf-flex bwf-h-14 bwf-items-center bwf-justify-center bwf-rounded-md bwf-bg-white bwf-p-1">
              <Image
                alt="logo"
                className="bwf-max-w-full"
                height={40}
                src="/images/logo-nasmoco.png"
                width={200}
              />
            </div>
            <div className="bwf-flex bwf-h-14 bwf-items-center bwf-justify-center bwf-rounded-md bwf-bg-white bwf-p-1">
              <Image
                alt="logo"
                className="bwf-max-w-full"
                height={40}
                src="/images/logo-nasmoco.png"
                width={200}
              />
            </div>
          </div>
        </div>
      </div>
      <Image
        alt="bottom-right-title"
        className="bwf-absolute bwf-bottom-0 bwf-right-0"
        height={200}
        src="/images/sv-bottom-right.png"
        width={220}
      />
    </section>
  );
};

export { FooterSection };
