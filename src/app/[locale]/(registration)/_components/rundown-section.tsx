/* eslint-disable tailwindcss/no-custom-classname */

'use client';

import { UpCircleFilled } from '@ant-design/icons';
import type { CollapseProps, PanelProps } from 'antd/es/collapse/Collapse';
import Collapse from 'antd/es/collapse/Collapse';
import Image from 'next/image';
import { useTranslations } from 'next-intl';

import { Button } from '@/app/components/ui/button';

import { Container } from './atoms/container';
import { Countdown } from './atoms/countdown';

const { Panel } = Collapse;

const items: CollapseProps['items'] = [
  {
    key: '1',
    label: (
      <div className="bwf-flex bwf-flex-col bwf-gap-1 bwf-font-sans bwf-tracking-wider">
        <p>Waktu & Tempat</p>
        <p className="bwf-text-base bwf-font-medium">HARI KE-1 & KE-2</p>
      </div>
    ),
    children: (
      <div className="bwf-flex bwf-flex-col bwf-gap-1 bwf-break-all bwf-font-sans">
        <p>Kamis - Jumat, 6 - 7 April 2023</p>
        <p>13.00 - 21.00 WIB</p>
        <p>MAC Ballroom</p>
        <p>Jl. Majapahit No.168, Gayamsari, Semarang</p>
      </div>
    ),
  },
  {
    key: '2',
    label: (
      <div className="bwf-flex bwf-flex-col bwf-gap-1 bwf-font-sans bwf-tracking-wider">
        <p>Waktu & Tempat</p>
        <p className="bwf-text-base bwf-font-medium">HARI KE-1 & KE-2</p>
      </div>
    ),
    children: (
      <p>
        <div className="bwf-flex bwf-flex-col bwf-gap-1 bwf-break-all">
          <p>Sabtu - Minggu, 8 - 9 April 2023</p>
          <p>10.00 - 21.00 WIB</p>
          <p>MAC Ballroom</p>
          <p>Jl. Majapahit No.168, Gayamsari, Semarang</p>
        </div>
      </p>
    ),
  },
];

const ExpandedIcon = (props: PanelProps) => {
  const { isActive } = props;
  return (
    <UpCircleFilled
      rotate={isActive ? 180 : 0}
      style={{ fontSize: '16px', color: '#c89559' }}
    />
  );
};

const RundownSection = () => {
  const t = useTranslations('global');
  const handleCreateEvent = () => {
    const eventName = encodeURIComponent('Blora Wedding Festival');
    const startDate = encodeURIComponent('2024-07-06T13:00:00');
    const endDate = encodeURIComponent('2024-07-09T21:00:00');
    const location = encodeURIComponent(
      'https://maps.app.goo.gl/bqvhnhtJMmfJEcvp6',
    );
    const timeZone = encodeURIComponent('Asia/Jakarta');
    const description = encodeURIComponent(t('metaDescription')); // Deskripsi acara

    const url = `https://calendar.google.com/calendar/u/0/r/eventedit?text=${eventName}&dates=${startDate}/${endDate}&location=${location}&ctz=${timeZone}&details=${description}`;
    window.open(url, '_blank');
  };

  return (
    <section className=" bwf-flex bwf-min-h-svh bwf-flex-col bwf-bg-red-50">
      <div className="bwf-relative bwf-bg-[url('/images/bg-green.webp')] bwf-px-8 bwf-py-11">
        <Image
          alt="top-left-title"
          className="bwf-absolute bwf-left-0 bwf-top-0"
          height={200}
          src="/images/sv-top-left.png"
          width={220}
        />
        <Container variant="rundown">
          <div className="bwf-flex bwf-flex-col bwf-items-center bwf-justify-center">
            <Image
              alt="logo-rwb"
              height={150}
              src="/images/rwb.png"
              width={150}
            />
          </div>

          <Collapse
            defaultActiveKey={['1']}
            expandIcon={ExpandedIcon}
            expandIconPosition="end"
            ghost
          >
            {items.map((item) => (
              <Panel
                key={item.key!.toString()}
                className="!bwf-rounded-none bwf-border-l-2 bwf-border-gold bwf-pl-2"
                header={item.label!}
              >
                {item.children!}
              </Panel>
            ))}
          </Collapse>
          <Button
            href="https://maps.app.goo.gl/qnZEfkq9HSL77a1A6"
            icon={
              <Image alt="Play" height={16} src="/images/maps.svg" width={16} />
            }
            target="_blank"
            type="link"
            variant="link"
          >
            {t('showLocation')}
          </Button>
        </Container>
        <Image
          alt="bottom-right-title"
          className="bwf-absolute bwf-bottom-0 bwf-right-0"
          height={200}
          src="/images/sv-bottom-right.png"
          width={220}
        />
      </div>
      <div className="bwf-flex bwf-min-h-[500px] bwf-flex-col bwf-justify-between bwf-bg-[url('/images/countdown-bg.webp')] bwf-bg-cover bwf-bg-center bwf-px-8 bwf-py-11">
        <Countdown targetDate="2024-06-30T23:59:59" />
        <Button
          className="!bwf-button-welcome !bwf-mx-auto !bwf-max-w-lg !bwf-animate-heartbeat"
          icon={
            <Image alt="Play" height={16} src="/images/alarm.svg" width={16} />
          }
          onClick={handleCreateEvent}
          size="rounded"
        >
          {t('remindMe')}
        </Button>
      </div>
    </section>
  );
};
export { RundownSection };
