/* eslint-disable tailwindcss/no-custom-classname */

'use client';

import { useTranslations } from 'next-intl';

import { Button } from '@/app/components/ui/button';

import { useRegisters } from '../_context/register-context';

const ButtonWelcome = () => {
  const t = useTranslations();
  const { isShowWelcomeMessage, handleSetIsShowWelcomeMessage } =
    useRegisters();

  if (isShowWelcomeMessage) return null;

  return (
    <Button
      className="!bwf-button-welcome !bwf-relative bwf-z-10 !bwf-animate-heartbeat"
      onClick={() => handleSetIsShowWelcomeMessage(true)}
    >
      {t('global.registerNow')}
    </Button>
  );
};

export { ButtonWelcome };
