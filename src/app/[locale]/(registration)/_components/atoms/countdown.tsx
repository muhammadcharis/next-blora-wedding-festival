import type { MessageKeys } from 'next-intl';
import { useTranslations } from 'next-intl';
import React, { useEffect, useState } from 'react';

interface TimeLeft {
  days?: number;
  hours?: number;
  minutes?: number;
  seconds?: number;
}

const Countdown = ({ targetDate }: { targetDate: string }) => {
  const t = useTranslations('global');
  const calculateTimeLeft = () => {
    const difference = +new Date(targetDate) - +new Date();
    let timeLeft: TimeLeft = {};

    if (difference > 0) {
      timeLeft = {
        days: Math.floor(difference / (1000 * 60 * 60 * 24)),
        hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
        minutes: Math.floor((difference / 1000 / 60) % 60),
        seconds: Math.floor((difference / 1000) % 60),
      };
    }

    return timeLeft;
  };

  const [timeLeft, setTimeLeft] = useState<TimeLeft>(calculateTimeLeft());

  useEffect(() => {
    const timer = setTimeout(() => {
      setTimeLeft(calculateTimeLeft());
    }, 1000);

    return () => clearTimeout(timer);
  });

  const timerComponents: JSX.Element[] = [];

  Object.entries(timeLeft).forEach(([interval, value]) => {
    if (value === undefined) {
      return;
    }

    timerComponents.push(
      <div
        key={interval}
        className="bwf-flex bwf-w-20 bwf-flex-col bwf-items-center bwf-justify-center bwf-gap-1 bwf-rounded-md bwf-border bwf-border-black bwf-bg-[#f8f0deff] bwf-px-1 bwf-py-3"
      >
        <div className="bwf-font-bold">
          {typeof value === 'number' && value > 0
            ? value.toString().padStart(2, '0')
            : '00'}
        </div>
        <div>{t(interval as MessageKeys<IntlMessages, 'global'>)}</div>
      </div>,
    );
  });

  if (!timerComponents.length) return null;

  return (
    <div className="bwf-flex bwf-flex-row bwf-items-center bwf-justify-center bwf-gap-3">
      {timerComponents}
    </div>
  );
};

export { Countdown };
