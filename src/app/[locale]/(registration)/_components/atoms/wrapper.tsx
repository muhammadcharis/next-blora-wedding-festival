/* eslint-disable react/jsx-no-useless-fragment */

'use client';

import React from 'react';

import { useRegisters } from '@/app/[locale]/(registration)/_context/register-context';

interface WrapperProps {
  children: Readonly<React.ReactNode>;
}

const Wrapper = ({ children }: WrapperProps) => {
  const { isShowWelcomeMessage } = useRegisters();

  if (!isShowWelcomeMessage) return null;

  return <>{children}</>;
};

export { Wrapper };
