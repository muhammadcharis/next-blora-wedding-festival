import React from 'react';

import { cn } from '@/libs/utils';

interface ContainerProps {
  children: Readonly<React.ReactNode>;
  variant: 'rsvp' | 'rundown';
  className?: string;
}

const Container = ({ children, variant, className }: ContainerProps) => {
  return (
    <div
      className={cn(
        'bwf-rounded-2xl bwf-border-4 bwf-border-gold-dark bwf-p-3 bwf-relative bwf-z-10 bwf-flex bwf-flex-col bwf-gap-4 ',
        variant === 'rundown' && 'bwf-bg-green-light',
        variant === 'rsvp' && 'bwf-bg-green-dark',
        className,
      )}
    >
      {children}
    </div>
  );
};

export { Container };
