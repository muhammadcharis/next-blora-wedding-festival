/* eslint-disable jsx-a11y/media-has-caption */

'use client';

import { ConfigProvider, FloatButton } from 'antd';
import Image from 'next/image';
import { useEffect, useRef, useState } from 'react';

import { useRegisters } from '../_context/register-context';

const AudioPlayer = () => {
  const { isShowWelcomeMessage } = useRegisters();
  const audioRef = useRef<HTMLAudioElement>(null);
  const [isPlaying, setIsPlaying] = useState(false);

  useEffect(() => {
    if (isShowWelcomeMessage && audioRef.current) {
      audioRef.current.play();
      setIsPlaying(true);
    }
  }, [isShowWelcomeMessage]);

  const handlePlayPause = () => {
    if (audioRef.current) {
      if (isPlaying) {
        audioRef.current.pause();
      } else {
        audioRef.current.play();
      }
      setIsPlaying(!isPlaying);
    }
  };

  return (
    <>
      <audio ref={audioRef} className="bwf-hidden" controls>
        <source src="/music/Dear God.mp4" type="audio/mp4" />
        Your browser does not support the audio element.
      </audio>
      <ConfigProvider
        theme={{
          token: {
            colorPrimary: '#c89559',
          },
        }}
      >
        <FloatButton
          icon={
            <Image
              alt="Play"
              height={100}
              src={
                isPlaying
                  ? '/images/music-note-svgrepo-com.svg'
                  : '/images/music-note-slash-svgrepo-com.svg'
              }
              width={100}
            />
          }
          onClick={handlePlayPause}
          shape="circle"
          style={{
            position: 'fixed',
            left: 10,
            bottom: 10,
          }}
          type="primary"
        />
      </ConfigProvider>
    </>
  );
};

export { AudioPlayer };
