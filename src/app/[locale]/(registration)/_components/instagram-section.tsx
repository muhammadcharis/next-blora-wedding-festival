import Image from 'next/image';

import { Button } from '@/app/components/ui/button';

const InstagramSection = () => {
  return (
    <section className="bwf-flex bwf-flex-col bwf-gap-6 bwf-py-11">
      <div className="bwf-flex bwf-flex-col bwf-gap-2 bwf-px-8">
        <h1 className="bwf-text-center bwf-text-2xl bwf-font-bold">
          Instagram Filter
        </h1>
        <p className="bwf-text-center bwf-font-light">
          Abadikan momen Anda selama menghadiri acara Diah Kharisma Wedding
          Bazaar dengan menggunakan Instagram filter di bawah
        </p>
      </div>
      <div className="bwf-inline-flex bwf-w-full bwf-gap-6 bwf-overflow-x-scroll bwf-whitespace-nowrap ">
        <div className="bwf-ml-11 bwf-flex bwf-flex-col bwf-items-center bwf-justify-center  bwf-gap-2">
          <Image
            alt="ig-filter"
            className="bwf-max-w-56  bwf-rounded-2xl"
            height={220}
            src="/images/ig-filter.jpg"
            width={220}
          />
          <Button className="!bwf-w-44 !bwf-rounded-full">
            Pakai Filter 1
          </Button>
        </div>

        <div className="bwf-mr-11 bwf-flex bwf-flex-col bwf-items-center bwf-justify-center  bwf-gap-2">
          <Image
            alt="ig-filter"
            className="bwf-max-w-56  bwf-rounded-2xl"
            height={220}
            src="/images/ig-filter-2.jpg"
            width={220}
          />
          <Button className="!bwf-w-44" size="rounded">
            Pakai Filter 2
          </Button>
        </div>
      </div>
    </section>
  );
};

export { InstagramSection };
