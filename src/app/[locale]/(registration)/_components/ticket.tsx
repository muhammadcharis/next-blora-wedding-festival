'use client';

import { saveAs } from 'file-saver';
import html2canvas from 'html2canvas';
import { useTranslations } from 'next-intl';
import { useCallback, useEffect, useRef, useState } from 'react';
import QRCode from 'react-qr-code';

import { Button } from '@/app/components/ui/button';
import { bwf } from '@/libs/fetch';
import { capitalizeEachWord, maskEmail, maskPhoneNumber } from '@/libs/text';

import { useRsvp } from '../_hooks/use-rsvp';
import type { Rsvp } from '../_schemas/rsvp';

interface TicketProps {
  hasAutoDownload?: boolean;
  saveToDatabase?: boolean;
}
const Ticket = ({
  hasAutoDownload = true,
  saveToDatabase = false,
}: TicketProps) => {
  const printRef = useRef<HTMLDivElement | null>(null);
  const { rsvpItems, updateRsvp, clearRsvp } = useRsvp();
  const [rsvp, setRsvp] = useState<Rsvp | undefined>();
  const [downloaded, setDownloaded] = useState(false);
  const [isUploading, setIsUploading] = useState(false);
  const [isDownloading, setIsDownloading] = useState(false);
  const t = useTranslations('global.rsvp');

  const updateRsvpData = useCallback(async () => {
    const rspv = rsvpItems.length ? rsvpItems[0] : undefined;
    setRsvp(rspv);
  }, [rsvpItems]);

  useEffect(() => {
    updateRsvpData();
  }, [updateRsvpData]);

  const handleSaveLocally = useCallback(
    async (blob: Blob) => {
      if (rsvp) {
        saveAs(blob, `${rsvp.name}.png`);
      }
    },
    [rsvp],
  );

  // Function to save image to database
  const handleUpload = useCallback(
    async (blob: Blob) => {
      if (rsvp) {
        const formData = new FormData();
        formData.append('image', blob);

        try {
          const response = await bwf.post(`/api/rsvp/${rsvp.id}`, {
            body: formData,
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          });

          const local: {
            [key: string]: any;
            rsvpUrl: string;
            phoneNumber: string;
            weedingPlanDate: string;
            createdAt: string;
          } = {
            ...response,
            rsvpUrl: response.rsvp_url,
            phoneNumber: response.phone_number,
            weedingPlanDate: response.wedding_planning_date,
            createdAt: response.created_at,
          };

          delete local.rsvp_url;
          delete local.phone_number;
          delete local.wedding_planning_date;
          delete local.created_at;

          updateRsvp(rsvp.id, local);
          handleSaveLocally(blob);
        } catch (error) {
          // console.error('Error saving image:', error);
        }
      }
    },
    [rsvp, handleSaveLocally, updateRsvp],
  );

  const handlePrint = useCallback(async () => {
    setIsDownloading(true);
    const printElement = printRef.current as HTMLElement | null | undefined;
    if (!printElement || !rsvp) return;

    await html2canvas(printElement).then((canvas: HTMLCanvasElement) => {
      canvas.toBlob(async (blob: Blob | null) => {
        if (blob) {
          if (saveToDatabase) {
            setIsUploading(true);
            await handleUpload(blob);
            setIsUploading(false);
          } else {
            await handleSaveLocally(blob);
          }
        }
      });
    });

    setTimeout(() => {
      setIsDownloading(false);
    }, 1000);
  }, [rsvp, handleSaveLocally, handleUpload, saveToDatabase]);

  useEffect(() => {
    if (rsvp && !downloaded && hasAutoDownload) {
      handlePrint();
      setDownloaded(true);
    }
  }, [rsvp, downloaded, handlePrint, hasAutoDownload]);

  if (!rsvp) return null;

  return (
    <div className="bwf-flex bwf-flex-col bwf-gap-4">
      <div
        ref={printRef}
        className="bwf-flex bwf-flex-col bwf-gap-2 bwf-rounded-lg bwf-border-4 bwf-border-green-dark bwf-bg-[url('/images/e-ticket.jpg')] bwf-bg-cover bwf-bg-center bwf-px-4 bwf-py-6"
      >
        <h2 className="bwf-text-center bwf-text-lg bwf-font-medium bwf-uppercase">
          {t('ticket.accessCard')}
        </h2>
        <p className="bwf-text-center bwf-text-sm">
          Diah Kharisma x MAC <br /> Ramadhan Wedding Bazaar 2023
        </p>
        <div className="bwf-mx-auto bwf-max-w-[200px]">
          <QRCode
            size={256}
            style={{
              height: 'auto',
              maxWidth: '100%',
              width: '100%',
            }}
            value={rsvp.id}
            viewBox="0 0 256 256"
          />
        </div>
        <div className="bwf-mt-1 bwf-flex bwf-flex-col bwf-gap-2 bwf-text-center">
          <div className="bwf-flex bwf-flex-col bwf-text-center">
            <p className="bwf-text-xs">{t('label.name')}</p>
            <p className="bwf-text-base">{capitalizeEachWord(rsvp.name!)}</p>
          </div>
          <div className="bwf-flex bwf-flex-col bwf-text-center">
            <p className="bwf-text-xs">{t('label.phoneNumber')}</p>
            <p className="bwf-text-base">
              {maskPhoneNumber(rsvp.phoneNumber!)}
            </p>
          </div>
          <div className="bwf-flex bwf-flex-col bwf-text-center">
            <p className="bwf-text-xs">{t('label.email')}</p>
            <p className="bwf-text-base">{maskEmail(rsvp.email!)}</p>
          </div>
        </div>
        <div className="bwf-mt-2 bwf-text-xs bwf-font-light bwf-text-red-700">
          <p className="bwf-text-sm bwf-uppercase">{t('ticket.important')}:</p>
          <ul className="bwf-list-disc bwf-pl-4">
            <li>{t('ticket.point1')}</li>
            <li>{t('ticket.point2')}</li>
          </ul>
        </div>
      </div>
      <div className="bwf-flex bwf-flex-col bwf-items-center bwf-justify-center bwf-gap-3 md:bwf-flex-row">
        <Button
          className="!bwf-w-44"
          loading={isDownloading}
          onClick={handlePrint}
          size="rounded"
        >
          {t('ticket.download')}
        </Button>
        <Button
          className="!bwf-w-44"
          disabled={isUploading}
          onClick={() => clearRsvp()}
          size="rounded"
        >
          {t('ticket.reset')}
        </Button>
      </div>
    </div>
  );
};

export { Ticket };
