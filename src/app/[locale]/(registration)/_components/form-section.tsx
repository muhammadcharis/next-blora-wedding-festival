/* eslint-disable @typescript-eslint/no-unsafe-call */

'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import {
  ConfigProvider,
  DatePicker,
  Form as AntdForm,
  notification,
} from 'antd';
import dayjs from 'dayjs';
import Image from 'next/image';
import type { MessageKeys } from 'next-intl';
import { useTranslations } from 'next-intl';
import { useMemo, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';

import { Button } from '@/app/components/ui/button';
import { FormLabel } from '@/app/components/ui/form-label';
import { Input } from '@/app/components/ui/input';
import { TextArea } from '@/app/components/ui/text-area';

import {
  isRsvp,
  type RsvpWithoutId,
  usePostRsvp,
} from '../_hooks/use-post-rsvp';
import { useRsvp } from '../_hooks/use-rsvp';
import { RsvpSchema } from '../_schemas/rsvp';
import { Container } from './atoms/container';
import { InstagramSection } from './instagram-section';
import { Ticket } from './ticket';

const FormSection = () => {
  const t = useTranslations('global');
  const dateFormatList = ['DD/MM/YYYY', 'DD/MM/YY', 'DD-MM-YYYY', 'DD-MM-YY'];
  const { rsvpItems, insertRsvp } = useRsvp();
  const [hasUploadingRsvp, setHasUploadingRsvp] = useState<boolean>(false);
  const { mutate, isPending } = usePostRsvp();

  const defaultValues: RsvpWithoutId = useMemo(
    () => ({
      address: undefined,
      name: undefined,
      email: undefined,
      phoneNumber: undefined,
      weedingPlanDate: undefined,
    }),
    [],
  );

  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm<RsvpWithoutId>({
    resolver: zodResolver(RsvpSchema),
    defaultValues,
  });

  const onHandleSubmit = async (rsvp: RsvpWithoutId) => {
    mutate(
      { rsvp },
      {
        onSuccess: async (data) => {
          if (isRsvp(data)) await insertRsvp(data);
          setHasUploadingRsvp(true);
          reset(defaultValues);
          notification.success({
            message: t('rsvp.success.title'),
            description: t('rsvp.success.description'),
          });
        },
      },
    );
  };

  return (
    <section className="bwf-flex bwf-flex-col bwf-gap-3 bwf-bg-[url('/images/form-rsvp.jpg')] bwf-bg-cover bwf-bg-center">
      <section className="bwf-p-8">
        <Container className="bwf-p-6" variant="rsvp">
          <div className="bwf-flex bwf-items-center bwf-justify-center">
            <Image
              alt="dahayu"
              className="bwf-invert"
              height={80}
              src="/images/dahayu.png"
              width={80}
            />
          </div>
          <p className="bwf-break-words bwf-text-center bwf-text-sm bwf-font-thin bwf-text-white md:bwf-text-base">
            {t('rsvpGreetings')}
          </p>
          {!rsvpItems.length && (
            <AntdForm
              className="!bwf-mt-3 !bwf-flex !bwf-flex-col !bwf-gap-2"
              layout="vertical"
              onFinish={handleSubmit(onHandleSubmit)}
            >
              <AntdForm.Item
                colon={false}
                help={
                  errors.name
                    ? t(
                        errors.name.message as MessageKeys<
                          IntlMessages,
                          'global'
                        >,
                      )
                    : null
                }
                label={<FormLabel label={t('rsvp.label.name')} />}
                style={{ marginBottom: 0 }}
                validateStatus={errors.name ? 'error' : ''}
              >
                <Controller
                  control={control}
                  name="name"
                  render={({ field }) => (
                    <Input
                      {...field}
                      inputMode="text"
                      placeholder={t('rsvp.placeholder.name')}
                    />
                  )}
                />
              </AntdForm.Item>

              <AntdForm.Item
                colon={false}
                help={
                  errors.phoneNumber
                    ? t(
                        errors.phoneNumber.message as MessageKeys<
                          IntlMessages,
                          'global'
                        >,
                      )
                    : null
                }
                label={<FormLabel label={t('rsvp.label.phoneNumber')} />}
                style={{ marginBottom: 0 }}
                validateStatus={errors.phoneNumber ? 'error' : ''}
              >
                <Controller
                  control={control}
                  name="phoneNumber"
                  render={({ field }) => (
                    <Input
                      {...field}
                      inputMode="text"
                      placeholder={t('rsvp.placeholder.phoneNumber')}
                    />
                  )}
                />
              </AntdForm.Item>

              <AntdForm.Item
                colon={false}
                help={
                  errors.email
                    ? t(
                        errors.email.message as MessageKeys<
                          IntlMessages,
                          'global'
                        >,
                      )
                    : null
                }
                label={<FormLabel label={t('rsvp.label.email')} />}
                style={{ marginBottom: 0 }}
                validateStatus={errors.email ? 'error' : ''}
              >
                <Controller
                  control={control}
                  name="email"
                  render={({ field }) => (
                    <Input
                      {...field}
                      inputMode="email"
                      placeholder={t('rsvp.placeholder.email')}
                    />
                  )}
                />
              </AntdForm.Item>

              <AntdForm.Item
                colon={false}
                help={
                  errors.weedingPlanDate
                    ? t(
                        errors.weedingPlanDate.message as MessageKeys<
                          IntlMessages,
                          'global'
                        >,
                      )
                    : null
                }
                label={<FormLabel label={t('rsvp.label.weedingPlanDate')} />}
                style={{ marginBottom: 0 }}
                validateStatus={errors.weedingPlanDate ? 'error' : ''}
              >
                <Controller
                  control={control}
                  name="weedingPlanDate"
                  render={({ field }) => (
                    <ConfigProvider
                      theme={{
                        token: {
                          colorPrimary: '#15423B',
                        },
                      }}
                    >
                      <DatePicker
                        allowClear
                        className="!bwf-w-full !bwf-resize-none !bwf-bg-green-light !bwf-py-2.5 !bwf-outline-none hover:!bwf-border-none hover:!bwf-outline-1 hover:!bwf-outline-offset-1 hover:!bwf-outline-gold-dark focus:!bwf-border-none focus:!bwf-outline-1 focus:!bwf-outline-offset-1 focus:!bwf-outline-gold"
                        format={dateFormatList}
                        onChange={(_, dateString) => {
                          field.onChange(dateString);
                        }}
                        placeholder={t('rsvp.placeholder.weedingPlanDate')}
                        value={
                          field.value
                            ? dayjs(field.value, 'DD/MM/YYYY')
                            : undefined
                        }
                      />
                    </ConfigProvider>
                  )}
                />
              </AntdForm.Item>

              <AntdForm.Item
                colon={false}
                help={
                  errors.address
                    ? t(
                        errors.address.message as MessageKeys<
                          IntlMessages,
                          'global'
                        >,
                      )
                    : null
                }
                label={<FormLabel label={t('rsvp.label.address')} />}
                style={{ marginBottom: 0 }}
                validateStatus={errors.address ? 'error' : ''}
              >
                <Controller
                  control={control}
                  name="address"
                  render={({ field }) => (
                    <TextArea
                      {...field}
                      placeholder={t('rsvp.placeholder.address')}
                    />
                  )}
                />
              </AntdForm.Item>

              <Button
                className="bwf-mx-auto !bwf-mt-4 bwf-w-2/3 !bwf-transform !bwf-border-none !bwf-bg-gold-dark !bwf-text-white !bwf-outline-none bwf-transition-transform bwf-duration-300 hover:bwf-scale-105 hover:!bwf-bg-gold-dark hover:!bwf-text-white"
                htmlType="submit"
                loading={isPending}
                size="rounded"
                type="default"
                variant="outline"
              >
                {t('register')}
              </Button>
            </AntdForm>
          )}
          {rsvpItems.length && (
            <div className="bwf-flex bwf-flex-col bwf-gap-2">
              <Ticket
                hasAutoDownload={hasUploadingRsvp}
                saveToDatabase={hasUploadingRsvp}
              />
            </div>
          )}
        </Container>
      </section>

      <InstagramSection />
    </section>
  );
};
export { FormSection };
