import { useLiveQuery } from 'dexie-react-hooks';

import { db } from '@/libs/db';

import type { Rsvp } from '../_schemas/rsvp';

const useRsvp = () => {
  const rsvpItems =
    useLiveQuery(async () => {
      const item = await db.rsvp.toArray();
      return item;
    }) ?? [];

  const insertRsvp = async (rsvp: Rsvp) => {
    const existingItem = await db.rsvp.get(rsvp.id);

    if (!existingItem) {
      await db.rsvp.add(rsvp);
    }
  };

  const updateRsvp = async (id: string, updatedData: Partial<Rsvp>) => {
    await db.rsvp.update(id, updatedData);
  };

  const clearRsvp = async () => {
    await db.rsvp.clear();
  };

  return {
    updateRsvp,
    insertRsvp,
    rsvpItems,
    clearRsvp,
  };
};

export { useRsvp };
