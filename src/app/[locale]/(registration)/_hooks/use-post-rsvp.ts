import { useMutation } from '@tanstack/react-query';
import dayjs from 'dayjs';

import { bwf } from '@/libs/fetch';

import type { Rsvp } from '../_schemas/rsvp';

export type RsvpWithoutId = Omit<Rsvp, 'id' | 'rsvpUrl'>;

interface PostOptions {
  rsvp: RsvpWithoutId;
}

const isRsvp = (data: any): data is Rsvp => {
  return (
    data &&
    typeof data === 'object' &&
    'id' in data &&
    typeof data.id === 'string'
  );
};

const postData = async ({ rsvp }: PostOptions) => {
  try {
    const payload: {
      [key: string]: any;
      phone_number: string;
      wedding_planning_date: Date;
    } = {
      ...rsvp,
      phone_number: rsvp.phoneNumber!,
      wedding_planning_date: new Date(
        dayjs(rsvp.weedingPlanDate!, 'DD/MM/YYYY').format('YYYY-MM-DD'),
      ),
    };

    delete payload.phoneNumber;
    delete payload.weedingPlanDate;

    const data = await bwf.post('/api/rsvp', { body: payload });

    const newRsvp: Rsvp = {
      id: data.id,
      rsvpUrl: '',
      ...rsvp,
    };
    return newRsvp;
  } catch (error) {
    return error;
  }
};

const usePostRsvp = () => {
  return useMutation({
    mutationKey: ['rsvp'],
    mutationFn: ({ rsvp }: PostOptions) => {
      return postData({ rsvp });
    },
  });
};

export { isRsvp, usePostRsvp };
