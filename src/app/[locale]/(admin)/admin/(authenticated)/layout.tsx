import { redirect } from 'next/navigation';

import { createClient } from '@/libs/superbase/server';

interface AuthLayoutProps {
  children: React.ReactNode;
}

const AuthLayout = async ({ children }: AuthLayoutProps) => {
  const supabase = createClient();

  const { error } = await supabase.auth.getUser();

  if (error) {
    redirect('/admin/login');
  }

  return children;
};

export default AuthLayout;
