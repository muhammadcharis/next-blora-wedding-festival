'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { Form as AntdForm } from 'antd';
import { useRouter } from 'next/navigation';
import type { MessageKeys } from 'next-intl';
import { useTranslations } from 'next-intl';
import { useMemo } from 'react';
import { Controller, useForm } from 'react-hook-form';

import { Button } from '@/app/components/ui/button';
import { FormLabel } from '@/app/components/ui/form-label';
import { Input } from '@/app/components/ui/input';
import { Password } from '@/app/components/ui/password';

import { useLogin } from '../_hooks/use-login';
import { type Login, LoginSchema } from '../_schemas/login';

const Form = () => {
  const { mutate, isPending } = useLogin();
  const t = useTranslations('login');
  const router = useRouter();
  const defaultValues: Login = useMemo(
    () => ({
      email: undefined,
      password: undefined,
    }),
    [],
  );

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<Login>({
    resolver: zodResolver(LoginSchema),
    defaultValues,
  });

  const onHandleSubmit = async (login: Login) => {
    mutate(
      { login },
      {
        onSuccess: () => {
          router.push('/admin');
        },
      },
    );
  };

  return (
    <AntdForm
      className="!bwf-mt-3 !bwf-flex !bwf-flex-col !bwf-gap-2"
      layout="vertical"
      onFinish={handleSubmit(onHandleSubmit)}
    >
      <AntdForm.Item
        colon={false}
        help={
          errors.email
            ? t(errors.email.message as MessageKeys<IntlMessages, 'login'>)
            : null
        }
        label={<FormLabel label={t('label.email')} variant="black" />}
        style={{ marginBottom: 0 }}
        validateStatus={errors.email ? 'error' : ''}
      >
        <Controller
          control={control}
          name="email"
          render={({ field }) => (
            <Input
              {...field}
              inputMode="email"
              placeholder={t('placeholder.email')}
              variantColor="white"
            />
          )}
        />
      </AntdForm.Item>

      <AntdForm.Item
        colon={false}
        help={
          errors.password
            ? t(errors.password.message as MessageKeys<IntlMessages, 'login'>)
            : null
        }
        label={<FormLabel label={t('label.password')} variant="black" />}
        style={{ marginBottom: 0 }}
        validateStatus={errors.email ? 'error' : ''}
      >
        <Controller
          control={control}
          name="password"
          render={({ field }) => (
            <Password
              {...field}
              inputMode="text"
              placeholder={t('placeholder.password')}
              variantColor="white"
            />
          )}
        />
      </AntdForm.Item>

      <Button
        className=" !bwf-mt-4 !bwf-transform !bwf-border-none !bwf-bg-gold-dark !bwf-text-white !bwf-outline-none bwf-transition-transform bwf-duration-300 hover:bwf-scale-105 hover:!bwf-bg-gold-dark hover:!bwf-text-white"
        htmlType="submit"
        loading={isPending}
        size="rounded"
        type="default"
        variant="outline"
      >
        {t('login')}
      </Button>
    </AntdForm>
  );
};

export { Form };
