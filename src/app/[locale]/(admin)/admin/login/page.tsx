import { Form } from './_components/form';

const Page = () => {
  return (
    <div className="bwf-h-60 bwf-bg-[url('/images/bg-green.webp')] bwf-bg-cover bwf-bg-center bwf-p-6">
      <div className="bwf-mx-auto bwf-max-w-md bwf-pt-12">
        <p className="bwf-text-white">Selamat datang kembali,</p>
        <h1 className="bwf-text-3xl bwf-font-bold bwf-text-white">
          Masuk dulu, yuk!
        </h1>
        <div className="bwf-mt-6 bwf-min-h-52 bwf-rounded-xl bwf-border bwf-border-green bwf-bg-white bwf-p-4">
          <Form />
        </div>
      </div>
    </div>
  );
};

export default Page;
