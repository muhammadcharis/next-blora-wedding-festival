import { useMutation } from '@tanstack/react-query';

import { bwf } from '@/libs/fetch';

import type { Login } from '../_schemas/login';

interface LoginOption {
  login: Login;
}

const postData = async ({ login }: LoginOption) => {
  try {
    const data = await bwf.post('/api/login', { body: login });

    console.log(data);
    return data;
  } catch (error) {
    return error;
  }
};

const useLogin = () => {
  return useMutation({
    mutationKey: ['login'],
    mutationFn: ({ login }: LoginOption) => {
      return postData({ login });
    },
  });
};

export { useLogin };
