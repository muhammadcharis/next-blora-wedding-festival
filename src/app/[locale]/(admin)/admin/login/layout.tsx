import { redirect } from 'next/navigation';

import { createClient } from '@/libs/superbase/server';

interface AuthLayoutProps {
  children: React.ReactNode;
}

const AuthLayout = async ({ children }: AuthLayoutProps) => {
  const supabase = createClient();

  const { data } = await supabase.auth.getUser();

  if (data.user) {
    redirect('/admin');
  }

  return children;
};

export default AuthLayout;
