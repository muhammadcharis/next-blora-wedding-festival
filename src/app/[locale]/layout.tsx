/* eslint-disable tailwindcss/no-custom-classname */
import '@/styles/global.css';

import { AntdRegistry } from '@ant-design/nextjs-registry';
import { Plus_Jakarta_Sans as FontSans } from 'next/font/google';
import { NextIntlClientProvider } from 'next-intl';
import { getMessages, getTranslations } from 'next-intl/server';

import { cn } from '@/libs/utils';

import { AppProviders } from './_providers/app-provider';

const fontSans = FontSans({
  subsets: ['latin'],
  variable: '--font-sans',
});

export const generateMetadata = async (props: RootLayoutProps) => {
  const t = await getTranslations({
    locale: props.params.locale,
    namespace: 'global',
  });

  return {
    title: t('metaTitle'),
    description: t('metaDescription'),
  };
};

interface RootLayoutProps {
  children: React.ReactNode;
  params: { locale: string };
}

const RootLayout = async ({
  children,
  params: { locale },
}: RootLayoutProps) => {
  const messages = await getMessages();
  return (
    <html lang={locale} suppressHydrationWarning>
      <body
        className={cn(
          'bwf-font-sans bwf-antialiased bwf-bg-gray-50',
          fontSans.variable,
        )}
      >
        <NextIntlClientProvider locale={locale} messages={messages}>
          <AntdRegistry>
            <AppProviders>{children}</AppProviders>
          </AntdRegistry>
        </NextIntlClientProvider>
      </body>
    </html>
  );
};

export default RootLayout;
