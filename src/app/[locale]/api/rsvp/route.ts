import type { NextRequest } from 'next/server';
import { NextResponse } from 'next/server';

import type { ErrorWithStatus } from '@/libs/fetch';
import { createResponseError } from '@/libs/route';
import { createClient } from '@/libs/superbase/server';

const POST = async (req: NextRequest) => {
  try {
    const body = await req.json();
    const supabase = createClient();
    const { data, error } = await supabase
      .from('rsvp')
      .insert(body)
      .select('id');

    if (error) {
      throw new Error('error while inserting');
    }
    return NextResponse.json(data[0]);
  } catch (error) {
    return createResponseError(error as ErrorWithStatus);
  }
};

export { POST };
