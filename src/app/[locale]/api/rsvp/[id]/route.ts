import { type NextRequest, NextResponse } from 'next/server';

import type { ErrorWithStatus } from '@/libs/fetch';
import { createResponseError } from '@/libs/route';
import { createClient } from '@/libs/superbase/server';

const POST = async (
  req: NextRequest,
  { params }: { params: { id: string } },
) => {
  try {
    const formData = await req.formData();
    const image = formData.get('image');
    const { id } = params;
    const supabase = createClient();

    const { error } = await supabase.storage
      .from('rsvp')
      .upload(`${id}/rsvp.png`, image!, {
        contentType: 'image/png',
        cacheControl: '3600',
        upsert: false,
      });

    if (error) {
      throw new Error('error while uploading');
    }

    const { data: publicUrlData } = supabase.storage
      .from('rsvp')
      .getPublicUrl(`${id}/rsvp.png`);

    const { publicUrl } = publicUrlData;

    const { data: dbData, error: dbError } = await supabase
      .from('rsvp')
      .update({ rsvp_url: publicUrl })
      .eq('id', id)
      .select();

    if (dbError) {
      throw new Error('error while saving URL to the database');
    }

    return NextResponse.json(dbData[0]);
  } catch (error) {
    return createResponseError(error as ErrorWithStatus);
  }
};

export { POST };
