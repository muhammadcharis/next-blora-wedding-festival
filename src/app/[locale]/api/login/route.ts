import type { NextRequest } from 'next/server';
import { NextResponse } from 'next/server';

import type { ErrorWithStatus } from '@/libs/fetch';
import { createResponseError } from '@/libs/route';
import { createClient } from '@/libs/superbase/server';

const POST = async (request: NextRequest) => {
  try {
    const body = await request.json();
    const supabase = createClient();

    const { error } = await supabase.auth.signInWithPassword(body);

    if (error) {
      console.log(error);
      throw new Error('error while login');
    }

    return NextResponse.json({ success: true });
  } catch (error) {
    return createResponseError(error as ErrorWithStatus);
  }
};

export { POST };
