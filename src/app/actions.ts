'use server';

import type { ResponseCookie } from 'next/dist/compiled/@edge-runtime/cookies';
import { cookies } from 'next/headers';

export const setCookie = async (
  key: string,
  value: string,
  option: Partial<ResponseCookie> = {},
) => {
  cookies().set(key, value, option);
};

export const getCookie = async (key: string) => {
  return cookies().get(key)?.value ?? '';
};

export const deleteCookie = async (key: string) => {
  cookies().delete(key);
};
