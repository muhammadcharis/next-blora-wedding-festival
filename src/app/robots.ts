import type { MetadataRoute } from 'next';

import { env } from '@/env.mjs';

const robots = (): MetadataRoute.Robots => {
  return {
    rules: { userAgent: '*', allow: '/' },
    sitemap: `${env.NEXT_PUBLIC_APP_URL}/sitemap.xml`,
  };
};

export default robots;
