import type { InputProps as AntdInputProps, InputRef } from 'antd';
import { Input as AntdInput } from 'antd';
import { cva } from 'class-variance-authority';
import React, { forwardRef } from 'react';

import { cn } from '@/libs/utils';

interface InputProp extends AntdInputProps {
  variantColor?: 'default' | 'white';
}

const inputVariants = cva(
  '!bwf-resize-none !bwf-py-2.5 !bwf-outline-none focus-within:!bwf-border-none focus-within:!bwf-outline-1 focus-within:!bwf-outline-gold hover:!bwf-border-none  hover:!bwf-outline-1 hover:!bwf-outline-offset-1 hover:!bwf-outline-gold-dark focus:!bwf-border-none focus:!bwf-outline-1  focus:!bwf-outline-offset-1 focus:!bwf-outline-gold',
  {
    variants: {
      variantColor: {
        default: '!bwf-bg-green-light',
        white: '!bwf-bg-white ',
      },
    },
    defaultVariants: {
      variantColor: 'default', // Ukuran default untuk ikon
    },
  },
);

const Input = forwardRef<InputRef, InputProp>(
  ({ variantColor = 'default', ...props }, ref) => {
    return (
      <AntdInput
        ref={ref}
        {...props}
        autoComplete="off" // Menggunakan nilai yang berbeda
        autoCorrect="off"
        className={cn(inputVariants({ variantColor }))}
        spellCheck="false" // Menonaktifkan spell check
      />
    );
  },
);

Input.displayName = 'Input';
export { Input };
