import { Input as AntdInput } from 'antd';
import type { TextAreaProps as AntdTextAreaProps } from 'antd/es/input/TextArea';
import type { Ref } from 'react';
import React, { forwardRef } from 'react';

type TextAreaProps = AntdTextAreaProps;

const TextArea = forwardRef<HTMLTextAreaElement, TextAreaProps>(
  (props, ref: Ref<HTMLTextAreaElement>) => {
    const { TextArea: AntdTextArea } = AntdInput;
    return (
      <AntdTextArea
        ref={ref}
        {...props}
        className="!bwf-resize-none !bwf-bg-green-light !bwf-py-2.5 !bwf-outline focus-within:!bwf-border-none focus-within:!bwf-outline-gold hover:!bwf-border-none hover:!bwf-outline-1 hover:!bwf-outline-offset-2 hover:!bwf-outline-gold-dark focus:!bwf-border-none focus:!bwf-shadow-none focus:!bwf-outline-1 focus:!bwf-outline-offset-2 focus:!bwf-outline-gold"
        rows={3}
      />
    );
  },
);

TextArea.displayName = 'TextArea';
export { TextArea };
