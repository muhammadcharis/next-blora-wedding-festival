/* eslint-disable tailwindcss/no-custom-classname */
import type { ButtonProps as AntdButtonProps } from 'antd';
import { Button as AntdButton } from 'antd';
import { cva } from 'class-variance-authority';
import React from 'react';

import { cn } from '@/libs/utils';

const buttonVariants = cva(
  '!bwf-ring-offset-background focus-visible:!bwf-ring-ring !bwf-inline-flex !bwf-cursor-pointer !bwf-items-center !bwf-justify-center !bwf-whitespace-nowrap !bwf-text-sm !bwf-font-medium !bwf-transition-colors focus-visible:!bwf-outline-none focus-visible:!bwf-ring-1 focus-visible:!bwf-ring-offset-0 disabled:!bwf-pointer-events-none disabled:!bwf-opacity-50',
  {
    variants: {
      variant: {
        default:
          '!bwf-bg-slate-700 !bwf-text-white !bwf-transition hover:!bwf-bg-black/90',
        destructive:
          '!bwf-bg-destructive !bwf-text-destructive-foreground hover:!bwf-bg-destructive/90',
        outline:
          '!bwf-border !bwf-border-black !bwf-outline hover:!bwf-bg-accent hover:!bwf-text-accent-foreground',
        secondary:
          '!bwf-bg-secondary !bwf-text-secondary-foreground hover:!bwf-bg-secondary/80',
        ghost:
          'hover:!bwf-border-black hover:!bwf-bg-accent hover:!bwf-text-accent-foreground',
        link: '!bwf-text-primary !bwf-underline-offset-4 hover:!bwf-underline',
      },
      size: {
        default: '!bwf-h-10 !bwf-px-8 !bwf-py-3',
        sm: '!bwf-h-9 !bwf-rounded-md !bwf-px-3',
        lg: '!bwf-h-11 !bwf-rounded-md !bwf-px-8',
        icon: '!bwf-h-10 !bwf-w-10',
        rounded: '!bwf-h-10 !bwf-rounded-full !bwf-px-8 !bwf-py-3',
      },
    },
    defaultVariants: {
      variant: 'default',
      size: 'default', // Ukuran default untuk ikon
    },
  },
);

interface ButtonProps
  extends Pick<
    AntdButtonProps,
    Exclude<keyof AntdButtonProps, 'size' | 'ghost'>
  > {
  variant?:
    | 'default'
    | 'destructive'
    | 'outline'
    | 'secondary'
    | 'ghost'
    | 'link';
  size?: 'default' | 'sm' | 'lg' | 'icon' | 'rounded';
}

const Button = ({
  className,
  variant = 'default',
  size = 'default',
  ...props
}: ButtonProps) => {
  return (
    <AntdButton
      {...props}
      className={cn(buttonVariants({ variant, size, className }))}
    >
      {props.children}
    </AntdButton>
  );
};

export { Button };
