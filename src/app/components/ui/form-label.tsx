import { cva } from 'class-variance-authority';

import { cn } from '@/libs/utils';

interface FormLabelProps {
  label: string;
  variant?: 'default' | 'black';
  isRequired?: boolean;
}

const labelVariants = cva('bwf-font-medium', {
  variants: {
    variant: {
      default: 'bwf-text-white',
      black: 'bwf-text-black',
    },
  },
  defaultVariants: {
    variant: 'default', // Ukuran default untuk ikon
  },
});

const FormLabel = ({
  label,
  variant = 'default',
  isRequired = true,
}: FormLabelProps) => {
  return (
    <span className={cn(labelVariants({ variant }))}>
      {label}
      {isRequired && <span className="bwf-text-red-500">*</span>}
    </span>
  );
};

export { FormLabel };
