/* eslint-disable react/no-unstable-nested-components */
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import type { InputProps as AntdInputProps, InputRef } from 'antd';
import { Input as AntdInput } from 'antd';
import { cva } from 'class-variance-authority';
import React, { forwardRef } from 'react';

import { cn } from '@/libs/utils';

interface PasswordProp extends AntdInputProps {
  variantColor?: 'default' | 'white';
}

const passwordVariants = cva('!bwf-resize-none !bwf-py-2.5', {
  variants: {
    variantColor: {
      default: '!bwf-bg-green-light',
      white: '!bwf-bg-white',
    },
  },
  defaultVariants: {
    variantColor: 'default', // Ukuran default untuk ikon
  },
});

const Password = forwardRef<InputRef, PasswordProp>(
  ({ variantColor = 'default', ...props }, ref) => {
    const { Password: AntdPassword } = AntdInput;

    return (
      <AntdPassword
        ref={ref}
        {...props}
        autoComplete="off" // Menggunakan nilai yang berbeda
        autoCorrect="off"
        className={cn(passwordVariants({ variantColor }))}
        iconRender={(visible) =>
          visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
        }
        spellCheck="false" // Menonaktifkan spell check
      />
    );
  },
);

Password.displayName = 'Password';
export { Password };
